int max=-1;
int indexOfSmallestElement(int array[],int i,int j){
	int currSmall=99999,index=0;
	for(;i<=j;i++){
		if(array[i]<currSmall){
			currSmall=array[i];
			index=i;
		}
	}
	return index;
}

void func(int array[],int i,int j){
	if(i>j){	
		return;
	}else{
		int a=indexOfSmallestElement(array,i,j);
		int x=array[a]*(j-i+1);
	//	printf("<%d,%d,%d--- %d....%d>  ",a,array[a],x,i+1,j+1);
		if(x>max)
			max=x;
		func(array,i,a-1);
		func(array,a+1,j);
	}
}

int main(){
	int array[]={2,1,5,6,2,3};
	func(array,0,5);
	printf("%d",max);
	return 0;
}

