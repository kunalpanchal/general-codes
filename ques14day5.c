/*14. Given 2 arrays find 2 elements one in each array whose sum is k*/

//Sorting using mergeSort

void merger(int *arr, int l, int m, int r)
{
    int size1 = m - l + 1,size2 =  r - m,i, j, k,sortedArray1[size1], sortedArray2[size2];
   
    for (i = 0; i < size1; i++)
        sortedArray1[i] = arr[l + i];
    for (j = 0; j < size2; j++)
        sortedArray2[j] = arr[m + 1+ j];
 
    i = 0;
    j = 0;
    k = l;
    while (i < size1 && j < size2)
    {
        if (sortedArray1[i] <= sortedArray2[j])
            arr[k++] = sortedArray1[i++];
        else
            arr[k++] = sortedArray2[j++];
    }
 
    while (i < size1)
    	arr[k++] = sortedArray1[i++];
    
    while (j < size2)
     	arr[k++] = sortedArray2[j++];
    
}
 
void mergeSort(int *arr, int l, int r)
{
   if (l < r)
   {
      int m = l+(r-l)/2; //Same as (l+r)/2 but avoids overflow for large l & h
      mergeSort(arr, l, m);
      mergeSort(arr, m+1, r);
      merger(arr, l, m, r);
   }
}
 
void printArray(int *A, int size)
{
    int i;
    for (i=0; i < size; i++)
        printf("%d ", A[i]);
    printf("\n");
}
 
 //End of Merge Sort
 
 void algo(int *a,int *b, int end1,int end2,int key){     //Complexity N
 	int i=0,j=end2;
	while(i<=end1&&j>=0){
		if(a[i]+b[j]==key){
		 printf("(%d)  element of first array and (%d)  element of second array equates to = %d",a[i],b[j],key);
		break;}
		else if(a[i]+b[j]>key) j--;
		else i++;
	}
 }
 
 int main()
{
    int arr1[] = {12, 11, 13, 5, 6, 7,-7,1,2,3,4,5,6};
   //  int arr1[] = {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,3};
    int size1 = sizeof(arr1)/sizeof(arr1[0]);
    int arr2[] = {5, 3, 7, 34, 6, 8,-2};
    int size2 = sizeof(arr2)/sizeof(arr2[0]);
  	int givenKey=18;
    printf("Given array1 : ");printArray(arr1, size1);
    printf("Given array2 : ");printArray(arr2, size2);
 	mergeSort(arr1, 0, size1 - 1);
 	mergeSort(arr2, 0, size2 - 1);
 	printf("Sorted array1 : ");printArray(arr1, size1);
 	printf("Sorted array2 : ");printArray(arr2, size2);
 	
 	algo(arr1,arr2,size1-1, size2 - 1,givenKey);
 	
	printf("\n\nComplexity : nlogn");
    return 0;
}
