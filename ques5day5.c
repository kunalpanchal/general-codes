/* Given an array with elements not in any particular order, arrange elements in the following ways (n log n )
a. a > b < c > d < e > f < g > h
b. a < b > c < d > e < f > g < h

*/

//Sorting using mergeSort

void merger(int *arr, int l, int m, int r)
{
    int size1 = m - l + 1,size2 =  r - m,i, j, k,sortedArray1[size1], sortedArray2[size2];
   
    for (i = 0; i < size1; i++)
        sortedArray1[i] = arr[l + i];
    for (j = 0; j < size2; j++)
        sortedArray2[j] = arr[m + 1+ j];
 
    i = 0;
    j = 0;
    k = l;
    while (i < size1 && j < size2)
    {
        if (sortedArray1[i] <= sortedArray2[j])
            arr[k++] = sortedArray1[i++];
        else
            arr[k++] = sortedArray2[j++];
    }
 
    while (i < size1)
    	arr[k++] = sortedArray1[i++];
    
    while (j < size2)
     	arr[k++] = sortedArray2[j++];
    
}
 
void mergeSort(int *arr, int l, int r)
{
   if (l < r)
   {
      int m = l+(r-l)/2; //Same as (l+r)/2 but avoids overflow for large l & h
      mergeSort(arr, l, m);
      mergeSort(arr, m+1, r);
      merger(arr, l, m, r);
   }
}
 
void printArray(int *A, int size)
{
    int i;
    for (i=0; i < size; i++)
        printf("%d ", A[i]);
    printf("\n");
}
 
 //End of Merge Sort
 
 void algo(int *a, int end){
 	int i=0;
	 for(;i<end;i++){
	 	int temp=a[i+1];
	 	a[i+1]=a[i];
	 	a[i]=temp;
	 	i++;
	 }
 }
 
 int main()
{
    int arr[] = {12, 11, 13, 5, 6, 7,-7};
    int size = sizeof(arr)/sizeof(arr[0]);
    printf("Given array : ");printArray(arr, size);
 	mergeSort(arr, 0, size - 1);
 	printf("Sorted array : ");printArray(arr, size);
 	algo(arr, size - 1);
 	printf("ZigZag array : ");printArray(arr, size);
 	printf("\n\nComplexity : nlogn");
    return 0;
}
