
#define size 7
struct stack {
   int s[size];
   int top;
} st;


void push(int item) {
   st.top++;
   st.s[st.top] = item;
}

int isEmpty() {
   if (st.top == -1)
      return 1;
   else
      return 0;
}

int pop() {
   int item;
   item = st.s[st.top];
   st.top--;
   return (item);
}


int currentSmall=999;
void sort(){
	if(st.top==-1){
		push(currentSmall);
	}else{
		int y=pop();
		if(y<currentSmall){
			currentSmall=y;
		}
		sort();
		push(y);
	}
}

void display() {
   int i;
   if (isEmpty())
      printf("\nStack Is Empty!");
   else {
      for (i = st.top; i >= 0; i--)
         printf(" %d", st.s[i]);
   }
}

int main(){
	st.top = -1;
	display();
	printf("\n");
	push(4);push(5);push(2);push(1);push(8);push(7);push(9);
	display();
	printf("\n");
	sort();
	display();
	return 0;
}


