/*12. Find the leader (Elements which have no greater element towards their right) in array*/

void printArray(int *A, int size){
    int i=0;
    for (; i < size; i++)
        printf("%d ", A[i]);
    printf("\n");
}

void algo(int *a,int size){
	int i=size,max=-9999;
	for(;i>=0;--i){
			if(a[i]>max){
				max=a[i];		
				printf(" {%d(%d)}",a[i],i);
			}
	}
}

int main(){
	int arr[] = {2,40,14,12, 11, 13, 5, 6, 7,-2,-7,-3};
    int size = sizeof(arr)/sizeof(arr[0]);
    printf("Given array : "); printArray(arr, size);
    printf("output : "); algo(arr, size - 1);
	return 0;
}
